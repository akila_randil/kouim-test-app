app.factory('errorStatusService', function () {
    var status = "";
    var data = "";
    return {
        setStatus: function (value) {
            status = value;
        },
        getStatus: function () {
            return status;
        },
        setData: function (value) {
            data = value;
        },
        getData: function () {
            return data;
        }
    }
});