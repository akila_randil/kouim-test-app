

var app = angular.module('ngOpenId',['ngRoute']);

app.config(['$routeProvider',function($routeProvider){
    $routeProvider
        .when('/',{templateUrl:'assets/src/landing_page.html'})
        .when('/roleMaps',{templateUrl:'assets/src/role_maps.html'})
        .when('/error', {templateUrl: 'assets/src/error_page.html'})
        .otherwise({redirectTo:'/'});
}]);
