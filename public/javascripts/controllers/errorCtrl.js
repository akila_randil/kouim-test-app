app.controller('errorCtrl', [
    '$scope',
    'errorStatusService',

    function ($scope, errorStatusService) {
        var status = errorStatusService.getStatus();
        var data = errorStatusService.getData();
        $scope.status = status;
        $scope.data = data;
    }]);

