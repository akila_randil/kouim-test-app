app.controller('roleMaps', [
    '$scope',
    '$http',
    '$location',
    'errorStatusService',

    function ($scope, $http, $location, errorStatusService) {
    $http.get('/getRoleMap')
    .success(function(data){
        $scope.data = data;
    })
        .error(function (data, status) {
            errorStatusService.setStatus(status);
            errorStatusService.setData(data);
            $location.path("/error");
    });
    }]);

