
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object main_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class main extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /*
* This template is called from the `index` template. This template
* handles the rendering of the page header and body tags. It takes
* two arguments, a `String` for the title of the page and an `Html`
* object to insert into the body of the page.
*/
  def apply/*7.2*/():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*7.4*/("""

"""),format.raw/*9.1*/("""<!DOCTYPE html>
<html lang="en" lang="en" data-ng-app="ngOpenId">
    <head>
        <title>OAuth Test App</title>
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-route.js"></script>
        <script src="assets/javascripts/app.js"></script>
        <script src="assets/javascripts/controllers/landingPageCtrl.js"></script>
        <script src="assets/javascripts/controllers/roleMapCtrl.js"></script>
        <script src="assets/javascripts/controllers/errorCtrl.js"></script>
        <script src="assets/javascripts/services/errorStatusService.js"></script>

    </head>
    <body>
        <div class="container" align="center" >
            <br/> <br/>
            <div ng-view></div>
        </div>

    </body>
</html>
"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}

/*
* This template is called from the `index` template. This template
* handles the rendering of the page header and body tags. It takes
* two arguments, a `String` for the title of the page and an `Html`
* object to insert into the body of the page.
*/
object main extends main_Scope0.main
              /*
                  -- GENERATED --
                  DATE: Thu Sep 01 16:17:35 IST 2016
                  SOURCE: /home/akilarh/MYComputer/WorkingDir/KOUIM Test App/KOUIM Test App/app/views/main.scala.html
                  HASH: 422499ab3203188ba539708c1a942145d2763b8d
                  MATRIX: 985->255|1081->257|1109->259
                  LINES: 32->7|37->7|39->9
                  -- GENERATED --
              */
          