package modules;

import com.google.inject.AbstractModule;
import services.ApplicationService;
import services.ApplicationServiceTask;
import services.impl.ApplicationServiceImpl;
import services.impl.ApplicationServiceTaskImpl;
import services.oidc.OpenIDService;
import services.oidc.handlers.request.AccessTokenRequestHandler;
import services.oidc.handlers.request.AuthenticationRequestHandler;
import services.oidc.handlers.request.RefreshTokenRequestHandler;
import services.oidc.handlers.request.claims.AuthorizationClaim;
import services.oidc.handlers.request.claims.TokenClaim;
import services.oidc.handlers.request.claims.impl.AuthorizationClaimImpl;
import services.oidc.handlers.request.claims.impl.TokenClaimImpl;
import services.oidc.handlers.request.impl.AccessTokenRequestHandlerImpl;
import services.oidc.handlers.request.impl.AuthenticationRequestHandlerImpl;
import services.oidc.handlers.request.impl.RefreshTokenRequestHandlerImpl;
import services.oidc.impl.OpenIDServiceImpl;

/**
 * Injection Dependency binding
 * @author Akila Randil Hettiarachchi
 */
public class Module extends AbstractModule {
    @Override
    protected void configure() {
        bind(ApplicationService.class).to(ApplicationServiceImpl.class).asEagerSingleton();

        bind(OpenIDService.class).to(OpenIDServiceImpl.class).asEagerSingleton();

        bind(ApplicationServiceTask.class).to(ApplicationServiceTaskImpl.class).asEagerSingleton();

        bind(AccessTokenRequestHandler.class).to(AccessTokenRequestHandlerImpl.class).asEagerSingleton();

        bind(RefreshTokenRequestHandler.class).to(RefreshTokenRequestHandlerImpl.class).asEagerSingleton();

        bind(AuthenticationRequestHandler.class).to(AuthenticationRequestHandlerImpl.class).asEagerSingleton();

        bind(AuthorizationClaim.class).to(AuthorizationClaimImpl.class).asEagerSingleton();

        bind(TokenClaim.class).to(TokenClaimImpl.class).asEagerSingleton();
    }
}
