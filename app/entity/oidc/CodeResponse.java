package entity.oidc;

import com.nimbusds.oauth2.sdk.AuthorizationCode;

/**
 * An entity to contain response details of Authorization Code request
 *
 * @author Akila Randil Hettiarachchi
 */
public class CodeResponse implements OIDCResponse {
    private AuthorizationCode code;

    public CodeResponse(AuthorizationCode code) {
        this.code = code;
    }

    public AuthorizationCode getCode() {
        return code;
    }

    public void setCode(AuthorizationCode code) {
        this.code = code;
    }
}
