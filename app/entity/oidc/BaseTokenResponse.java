package entity.oidc;

import com.nimbusds.oauth2.sdk.token.AccessToken;
import com.nimbusds.oauth2.sdk.token.RefreshToken;

import java.time.Instant;

/**
 * An entity to contain response details of Access and Response token requests
 *
 * @author Akila Randil Hettiarachchi
 */
public class BaseTokenResponse implements OIDCResponse {

    private AccessToken accessToken;
    private Instant responseInitTime;
    private long lifeTime;
    private RefreshToken refreshToken;

    public BaseTokenResponse(AccessToken accessToken, Instant responseInitTime) {
        this.accessToken = accessToken;
        this.responseInitTime = responseInitTime;
    }

    public BaseTokenResponse(AccessToken accessToken, Instant responseInitTime, long lifeTime, RefreshToken refreshToken) {
        this.accessToken = accessToken;
        this.responseInitTime = responseInitTime;
        this.lifeTime = lifeTime;
        this.refreshToken = refreshToken;
    }

    public AccessToken getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(AccessToken accessToken) {
        this.accessToken = accessToken;
    }

    public Instant getResponseInitTime() {
        return responseInitTime;
    }

    public void setResponseInitTime(Instant responseInitTime) {
        this.responseInitTime = responseInitTime;
    }

    public long getLifeTime() {
        return lifeTime;
    }

    public void setLifeTime(long lifeTime) {
        this.lifeTime = lifeTime;
    }

    public RefreshToken getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(RefreshToken refreshToken) {
        this.refreshToken = refreshToken;
    }
}
