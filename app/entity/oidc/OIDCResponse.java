package entity.oidc;

/**
 * A common interface for Authorization Code and Token responses
 *
 * @author Akila Randil Hettiarachchi
 */
public interface OIDCResponse {
}
