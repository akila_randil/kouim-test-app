package entity;

import java.util.Map;

/**
 * An entity to contain necessary details of response from the resource server
 * @author Akila Randil Hettiarachchi
 */
public class ResourceResponse {
    private String body;
    private int status;
    private Map<String, String> headers;


    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }
}
