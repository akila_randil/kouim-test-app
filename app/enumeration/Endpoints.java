package enumeration;

/**
 * Endpoints of the Authorization Server
 * @author Akila Randil Hettiarachchi
 */
public enum Endpoints {

    AUTHORIZATION_ENDPOINT("http://google.com/"),
    ACCESS_TOKEN_ENDPOINT("http://google.com/"),
    USERINFO_ENDPOINT("");

    private final String text;

    Endpoints(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
