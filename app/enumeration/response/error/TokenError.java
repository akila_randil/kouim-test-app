package enumeration.response.error;

import com.nimbusds.oauth2.sdk.ErrorObject;
import com.nimbusds.oauth2.sdk.OAuth2Error;
import exception.ResponseException;
import play.mvc.Results;
import services.oidc.handlers.error.ErrorHandlerCommand;

import java.util.HashMap;
import java.util.Map;

/**
 * Enum to handle execution of outputs based on different error code of the Token Response based on
 * OAuth2.0 specification
 *
 * @author Akila Randil Hettiarachchi
 */
public enum TokenError {

    INVALID_REQUEST(OAuth2Error.INVALID_REQUEST, () -> Results.badRequest("Token Error - 'Invalid Request' ")),
    INVALID_CLIENT(OAuth2Error.INVALID_CLIENT, () -> Results.unauthorized("Token Error - 'Invalid Client'")),
    INVALID_GRANT(OAuth2Error.INVALID_GRANT, () -> Results.unauthorized("Token Error - 'Invalid Grant'")),
    UNAUTHORIZED_GRANT(OAuth2Error.UNAUTHORIZED_CLIENT, () -> Results.badRequest("Token Error - 'Unsupported Grant'")),
    UNSUPPORTED_GRANT_TYPE(OAuth2Error.UNSUPPORTED_GRANT_TYPE, () -> Results.badRequest("Token Error - 'Unsupported Grant Type'")),
    INVALID_SCOPE(OAuth2Error.INVALID_SCOPE, () -> Results.badRequest("Token Error - 'Invalid Scope'"));

    private static Map<ErrorObject, TokenError> commandMap;

    static {
        commandMap = new HashMap<>();
        for (TokenError e : TokenError.values()) {
            commandMap.put(e.getErrorObject(), e);
        }
    }

    private ErrorObject errorObject;
    private ErrorHandlerCommand command;

    TokenError(ErrorObject errorObject, ErrorHandlerCommand command) {
        this.errorObject = errorObject;
        this.command = command;
    }

    public static ErrorHandlerCommand getCommand(ErrorObject errorObject) throws ResponseException {
        if (!commandMap.containsKey(errorObject)) {
            throw new ResponseException("Error Code not found " + errorObject.getCode());
        }
        return commandMap.get(errorObject).getCommand();
    }

    public ErrorObject getErrorObject() {
        return errorObject;
    }

    public ErrorHandlerCommand getCommand() {
        return command;
    }

    public String getValue() {
        return errorObject.getCode();
    }
}
