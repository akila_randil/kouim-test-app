package enumeration.response.error;

import com.nimbusds.oauth2.sdk.ErrorObject;
import com.nimbusds.oauth2.sdk.OAuth2Error;
import com.nimbusds.openid.connect.sdk.OIDCError;
import controllers.routes;
import exception.ResponseException;
import play.mvc.Results;
import services.oidc.handlers.error.ErrorHandlerCommand;

import java.util.HashMap;
import java.util.Map;

/**
 * Enum to handle execution of outputs based on different error codes of the Authentication response based
 * on OpenID connect 1.0 and OAuth 2.0 specification
 *
 * @author Akila Randil Hettiarachchi
 */
public enum AuthenticationError {
    INVALID_REQUEST(OAuth2Error.INVALID_REQUEST, () -> Results.badRequest("Authentication Error - 'Invalid Request' ")),
    UNAUTHORIZED_CLIENT(OAuth2Error.UNAUTHORIZED_CLIENT, () -> Results.badRequest("Authentication Error - 'Unauthorized Client' ")),
    ACCESS_DENIED(OAuth2Error.ACCESS_DENIED, () -> Results.forbidden("Authentication Error - 'Access Denied' ")),
    UNSUPPORTED_RESPONSE_TYPE(OAuth2Error.UNSUPPORTED_RESPONSE_TYPE, () -> Results.badRequest("Authentication Error - 'Unsupported Response Type' ")),
    INVALID_SCOPE(OAuth2Error.INVALID_SCOPE, () -> Results.badRequest("Authentication Error - 'Invalid Scope' ")),
    SERVER_ERROR(OAuth2Error.SERVER_ERROR, () -> Results.internalServerError("Authentication Error - 'Server Error' ")),
    TEMPORARILY_UNAVAILABLE(OAuth2Error.TEMPORARILY_UNAVAILABLE, () -> Results.internalServerError("Authentication Error - 'Temporarily Unavailable' ")),
    INTERACTION_REQUIRED(OIDCError.INTERACTION_REQUIRED, () -> Results.found(routes.ApplicationController.login())),
    LOGIN_REQUIRED(OIDCError.LOGIN_REQUIRED, () -> Results.found(routes.ApplicationController.login())),
    ACCOUNT_SELECTION_REQUIRED(OIDCError.ACCOUNT_SELECTION_REQUIRED, () -> Results.found(routes.ApplicationController.login())),
    CONSENT_REQUIRED(OIDCError.CONSENT_REQUIRED, () -> Results.found(routes.ApplicationController.login())),
    INVALID_REQUEST_URI(OIDCError.INVALID_REQUEST_URI, () -> Results.found(routes.ApplicationController.login())),
    INVALID_REQUEST_OBJECT(OIDCError.INVALID_REQUEST_OBJECT, () -> Results.found(routes.ApplicationController.login())),
    REQUEST_NOT_SUPPORTED(OIDCError.REQUEST_NOT_SUPPORTED, () -> Results.found(routes.ApplicationController.login())),
    REQUEST_URI_NOT_SUPPORTED(OIDCError.REQUEST_URI_NOT_SUPPORTED, () -> Results.found(routes.ApplicationController.login())),
    REGISTRATION_NOT_SUPPORTED(OIDCError.REGISTRATION_NOT_SUPPORTED, () -> Results.found(routes.ApplicationController.login()));

    private static Map<ErrorObject, AuthenticationError> commandsMap;

    static {
        commandsMap = new HashMap<>();
        for (AuthenticationError a : AuthenticationError.values()) {
            commandsMap.put(a.getErrorObject(), a);
        }
    }

    private ErrorObject error;
    private ErrorHandlerCommand command;

    AuthenticationError(ErrorObject error, ErrorHandlerCommand command) {
        this.error = error;
        this.command = command;
    }

    public static ErrorHandlerCommand getCommand(ErrorObject error) throws ResponseException {
        if (!commandsMap.containsKey(error)) {
            throw new ResponseException("Error code not found " + error.getCode());
        }
        return commandsMap.get(error).getCommand();

    }

    public ErrorObject getErrorObject() {
        return error;
    }

    public ErrorHandlerCommand getCommand() {
        return command;
    }

    public String getValue() {
        return error.getCode();
    }


}

