package enumeration.response.success;

/**
 * Claims for the OpenID Connect 1.0 Access Token Successful Responses
 * @author Akila Randil Hettiarachchi
 */
public enum AccessTokenClaim {
    ACCESS_TOKEN("access_token"),
    TOKEN_TYPE("token_type"),
    REFRESH_TOKEN("refresh_token"),
    EXPIRES_IN("expires_in"),
    ID_TOKEN("id_token");


    private final String text;

    AccessTokenClaim(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
