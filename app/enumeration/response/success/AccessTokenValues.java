package enumeration.response.success;

/**
 * Values for the OpenID Connect 1.0 Access Token Successful Responses
 * @author Akila Randil Hettiarachchi
 */
public enum AccessTokenValues {
    BEARER("Bearer");


    private final String text;

    AccessTokenValues(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
