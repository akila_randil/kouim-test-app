package enumeration.claims.oauth;

/**
 * Claims for the OAuth 2.0 Authorization Code Flow
 * @author Akila Randil Hettiarachchi
 */
public enum OauthClaims {

    REDIRECT_URI("redirect_uri"),
    RESPONSE_TYPE("response_type"),
    CLIENT_ID("client_id"),
    SCOPE("scope"),
    STATE("state"),
    CODE("code"),
    GRANT_TYPE("grant_type"),
    REFRESH_TOKEN("refresh_token");

    private final String text;

    OauthClaims(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
