package enumeration.claims.oauth;

/**
 * Values for the OAuth 2.0 Claims
 * @author Akila Randil Hettiarachchi
 */
public enum OauthValues {
    NAME("testapp"),
    RESPONSE_TYPE("code"),
    SCOPE("openid"),
    REDIRECT_URI("http://www.client.com/auth"),
    CLIENT_ID("15654"),
    REFRESH_TOKEN_GRANNT_TYPE("refresh_token"),
    ACCESS_TOKEN_GRANT_TYPE("authorization_code");


    private final String text;

    OauthValues(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }

}
