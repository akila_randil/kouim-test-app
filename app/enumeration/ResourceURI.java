package enumeration;

/**
 * URIs of the Resource Server
 * @author Akila Randil Hettiarachchi
 */
public enum ResourceURI {

    LOGIN("http://login.google.com"),
    ROLE_MAPS("http://www.google.com"),
    USERS("http://google.com"),
    ISSUER("http://google.com");

    private final String text;

    ResourceURI(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
