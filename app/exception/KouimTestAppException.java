package exception;

import com.nimbusds.oauth2.sdk.ErrorObject;

/**
 * General Exception for the test app
 * @author Akila Randil Hettiarachchi
 */
public class KouimTestAppException extends Exception {

    public KouimTestAppException() {
        super();
    }

    public KouimTestAppException(String message) {
        super(message);
    }

    public KouimTestAppException(String message, Throwable cause) {
        super(message, cause);
    }

    public KouimTestAppException(Throwable cause) {
        super(cause);
    }

    protected KouimTestAppException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }


}
