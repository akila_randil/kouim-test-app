package exception;

import com.nimbusds.oauth2.sdk.ErrorObject;

/**
 * Exception for the responses
 * @author Akila Randil Hettiarachchi
 */
public class ResponseException extends KouimTestAppException {

    public ResponseException() {
        super();
    }

    public ResponseException(String message) {
        super(message);
    }

    public ResponseException(String message, Throwable cause) {
        super(message, cause);
    }

    public ResponseException(Throwable cause) {
        super(cause);
    }

    protected ResponseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }


}
