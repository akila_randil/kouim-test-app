package services;

import entity.ResourceResponse;
import exception.KouimTestAppException;

/**
 * Interface for the Application Service for data requests to the resource server
 *
 * @author Akila Randil Hettiarachchi
 */
@FunctionalInterface
public interface ApplicationService {


    /**
     * Request to the resource server that returns a response
     *
     * @param url resource servere data url
     * @return a {@link ResourceResponse} instance containing the response data
     * @throws KouimTestAppException if the request handling fails
     */
    ResourceResponse request(String url) throws KouimTestAppException;
}
