package services.impl;

import com.google.inject.Inject;
import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import entity.ResourceResponse;
import enumeration.response.success.AccessTokenClaim;
import exception.KouimTestAppException;
import services.ApplicationServiceTask;
import services.oidc.OpenIDService;

import java.io.IOException;
import java.net.URL;
import java.time.Duration;
import java.time.Instant;

import static play.mvc.Controller.session;

/**
 * Application Service Task Implementation
 *
 * @author Akila Randil Hettiarachchi
 */
public class ApplicationServiceTaskImpl implements ApplicationServiceTask {

    private String url;
    private ResourceResponse resourceResponse;
    private OpenIDService openIDService;

    @Inject
    public ApplicationServiceTaskImpl(OpenIDService openIDService) {
        this.openIDService = openIDService;
        resourceResponse = new ResourceResponse();
    }


    @Override
    public ResourceResponse getData() throws KouimTestAppException {
        try {
            HTTPRequest request = new HTTPRequest(HTTPRequest.Method.GET, new URL(url));
            request.setContentType("application/json");
//            if there is no access token
            if (session(AccessTokenClaim.ACCESS_TOKEN.toString()) == null) {
                openIDService.accessTokenRequest();
                request.setAuthorization(session(AccessTokenClaim.ACCESS_TOKEN.toString()));
            } else {

                this.timeOutValidator();
                request.setAuthorization(session(AccessTokenClaim.ACCESS_TOKEN.toString()));
            }

            HTTPResponse httpResponse = request.send();
            return this.responseErrorValidator(httpResponse);

        } catch (IOException | ParseException e) {
            throw new KouimTestAppException(e);
        }
    }

    @Override
    public void setUrl(String url) {
        this.url = url;
    }

    //    the error responses from the resource server
    private ResourceResponse responseErrorValidator(HTTPResponse httpResponse) throws KouimTestAppException {
        resourceResponse.setBody(httpResponse.getContent());
        resourceResponse.setStatus(httpResponse.getStatusCode());
        resourceResponse.setHeaders(httpResponse.getHeaders());

        // checks if an invalid request code has been sent from the resource server.
        // need to know what sort of errors the server will response . afterwards the necessary validations can be done here
        if ("error".equalsIgnoreCase(httpResponse.getContent())) {
            throw new KouimTestAppException("value not found or not authorized");
        }
        return resourceResponse;
    }

    //validates the timeout
    private void timeOutValidator() throws KouimTestAppException {
        long lifeTime = openIDService.getLifeTime();
        Instant responseInitTime = openIDService.getResponseInitTime();
        Instant currentTime = Instant.now();

        long timeDiff = Duration.between(responseInitTime, currentTime).toMillis();
        if (timeDiff > lifeTime) {
            openIDService.refreshTokenRequest();
        } else {
            openIDService.accessTokenRequest();
        }
    }
}
