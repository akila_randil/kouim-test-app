package services.impl;

import com.google.inject.Inject;
import entity.ResourceResponse;
import exception.KouimTestAppException;
import services.ApplicationService;
import services.ApplicationServiceTask;

/**
 * Application Service implementation
 * @author Akila Randil Hettiarachchi
 */
public class ApplicationServiceImpl implements ApplicationService {

    private ApplicationServiceTask task;

    @Inject
    public ApplicationServiceImpl(ApplicationServiceTask task) throws KouimTestAppException {
        try {
            this.task = task;
        } catch (NullPointerException e) {
            throw new KouimTestAppException(e);
        }


    }

    @Override
    public ResourceResponse request(String url) throws KouimTestAppException {

        task.setUrl(url);
        return task.getData();

    }
}