package services;

import entity.ResourceResponse;
import exception.KouimTestAppException;

/**
 * A task that handles the requests and responses sent to the resource server
 *
 * @author Akila Randil Hettiarachchi
 */
public interface ApplicationServiceTask {

    /**
     * Sets the url
     *
     * @param url specific url of the resource server data that is needed
     */
    void setUrl(String url);

    /**
     * Request and response handling sent to the resource server
     *
     * @return a {@link ResourceResponse} instance that contains the details of the a successful response
     * @throws KouimTestAppException if the resource server returns an error
     */
    ResourceResponse getData() throws KouimTestAppException;
}
