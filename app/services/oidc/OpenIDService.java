package services.oidc;

import com.nimbusds.oauth2.sdk.AuthorizationCode;
import exception.KouimTestAppException;

import java.time.Instant;

/**
 * An interface to implement the OpenIDService for Authorization Code Flow ans session management
 *
 * @author Akila Randil Hettiarachchi
 */
public interface OpenIDService {

    /**
     * Gets the code from the authorization server and puts it the <code>session("user_session")</code>
     *
     * @throws KouimTestAppException if code response fails
     */
    void codeRequest() throws KouimTestAppException;

    /**
     * Retrieves the AccessToken and other claims using the Authorization code in the <code>session("user_session")</code>
     * and puts the access token value in the <code>session("access_token")</code> and the refresh token  value in the
     * <code>session("refresh_token")</code>
     *
     * @throws KouimTestAppException if the access token response fails
     */
    void accessTokenRequest() throws KouimTestAppException;

    /**
     * Retrieves a new AccessToken using the Refresh Token in the <code>session("refresh_token")</code> value and
     * replaces the <code>session("access_token")</code> value and the <code>session("refresh_token")</code> value
     * with the new ones.
     *
     * @throws KouimTestAppException if the refresh token response fails
     */
    void refreshTokenRequest() throws KouimTestAppException;

    /**
     * Returns the life time of the access token
     *
     * @return lifetime in long
     */
    long getLifeTime();

    /**
     * Returns the initial time of the response
     *
     * @return an {@link Instant} instance
     */
    Instant getResponseInitTime();

    /**
     * Returns the authorization code
     *
     * @return an {@link AuthorizationCode} instance
     */
    AuthorizationCode getCode();
}
