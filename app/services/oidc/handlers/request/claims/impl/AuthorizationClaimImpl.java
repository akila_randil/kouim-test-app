package services.oidc.handlers.request.claims.impl;

import com.google.inject.Inject;
import com.nimbusds.oauth2.sdk.id.ClientID;
import services.oidc.handlers.request.claims.AuthorizationClaim;

import java.net.URI;

/**
 * Unit to contain Authorization Code request claims
 *
 * @author Akila Randil Hettiarachchi
 */
public class AuthorizationClaimImpl implements AuthorizationClaim {

    private ClientID clientID;
    private URI redirectionUri;

    @Inject
    public AuthorizationClaimImpl() {
        // default constructor
    }

    @Override
    public ClientID getClientID() {
        return clientID;
    }

    @Override
    public void setClientID(ClientID clientID) {
        this.clientID = clientID;
    }

    @Override
    public URI getRedirectionUri() {
        return redirectionUri;
    }

    @Override
    public void setRedirectionUri(URI redirectionUri) {
        this.redirectionUri = redirectionUri;
    }

}
