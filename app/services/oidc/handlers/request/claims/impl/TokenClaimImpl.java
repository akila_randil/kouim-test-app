package services.oidc.handlers.request.claims.impl;

import com.google.inject.Inject;
import com.nimbusds.oauth2.sdk.AuthorizationGrant;
import com.nimbusds.oauth2.sdk.id.ClientID;
import services.oidc.handlers.request.claims.TokenClaim;

/**
 * Unit to contain Token request claims
 *
 * @author Akila Randil Hettiarachchi
 */
public class TokenClaimImpl implements TokenClaim {

    private ClientID clientID;
    private AuthorizationGrant authorizationGrant;

    @Inject
    public TokenClaimImpl() {
        // default constructor
    }

    @Override
    public ClientID getClientID() {
        return clientID;
    }

    @Override
    public void setClientID(ClientID clientID) {
        this.clientID = clientID;
    }

    @Override
    public AuthorizationGrant getAuthorizationGrant() {
        return authorizationGrant;
    }

    @Override
    public void setAuthorizationGrant(AuthorizationGrant authorizationGrant) {
        this.authorizationGrant = authorizationGrant;
    }
}
