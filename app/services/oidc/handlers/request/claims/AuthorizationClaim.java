package services.oidc.handlers.request.claims;

import java.net.URI;

/**
 * Interface that has methods for Authorization request claims
 *
 * @author Akila Randil Hettiarachchi
 */
public interface AuthorizationClaim extends Claims {

    /**
     * Returns the redirection URI
     *
     * @return redirection URI
     */
    URI getRedirectionUri();

    /**
     * Sets the redirection URI
     *
     * @param redirectionUri URI
     */
    void setRedirectionUri(URI redirectionUri);
}
