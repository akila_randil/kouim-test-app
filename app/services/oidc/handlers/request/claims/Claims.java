package services.oidc.handlers.request.claims;

import com.nimbusds.oauth2.sdk.id.ClientID;

/**
 * Interface to contain common methods for Token and Authorization Code request claims
 *
 * @author Akila Randil Hettiarachchi
 */
public interface Claims {
    /**
     * Returns the client id
     *
     * @return instance of {@link ClientID}
     */
    ClientID getClientID();

    /**
     * Sets the client id
     *
     * @param clientID Client id
     */
    void setClientID(ClientID clientID);
}
