package services.oidc.handlers.request.claims;

import com.nimbusds.oauth2.sdk.AuthorizationGrant;


/**
 * Interface to have common methods for Access and Refresh token claims
 *
 * @author Akila Randil Hettiarachchi
 */
public interface TokenClaim extends Claims {


    /**
     * Returns the Authorization Grant type
     *
     * @return instance of {@link AuthorizationGrant}
     */
    AuthorizationGrant getAuthorizationGrant();

    /**
     * Set the Authorization Grant type
     *
     * @param authorizationGrant grant type of request
     */
    void setAuthorizationGrant(AuthorizationGrant authorizationGrant);
}
