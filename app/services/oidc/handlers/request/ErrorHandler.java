package services.oidc.handlers.request;

import com.nimbusds.oauth2.sdk.ErrorResponse;
import entity.oidc.OIDCResponse;
import exception.KouimTestAppException;
import exception.ResponseException;
import play.mvc.Result;

/**
 * An error handling interface for OAuth 2.0 error responses
 *
 * @author Akila Randil Hettiarachchi
 */
@FunctionalInterface
public interface ErrorHandler extends OIDCResponse {

    /**
     * Error handler for the responses of the Authorization Code Flow requests
     *
     * @param errorResponse an {@link ErrorResponse} instance containing all the error claims and values
     * @throws ResponseException  if the error response fails
     */
    Result errorHandler(ErrorResponse errorResponse) throws KouimTestAppException;
}
