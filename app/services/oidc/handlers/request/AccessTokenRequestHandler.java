package services.oidc.handlers.request;

/**
 * An interface for AccessToken Request handling
 *
 * @author Akila Randil Hettiarachchi
 */
public interface AccessTokenRequestHandler extends RequestHandler {
}
