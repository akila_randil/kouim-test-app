package services.oidc.handlers.request;

import entity.oidc.OIDCResponse;
import exception.KouimTestAppException;

/**
 * A common interface for OAuth 2.0 requests handling
 *
 * @author Akila Randil Hettiarachchi
 */
public interface RequestHandler extends ErrorHandler {

    /**
     * Handle Request for different types of requests in the Authorization Code
     * flow. The integration of the {@link com.nimbusds} api happens here.
     *
     * @return an {@link OIDCResponse} with the necessary response details
     * @throws KouimTestAppException if the handling process fails
     */
    OIDCResponse handleRequest() throws KouimTestAppException;


}
