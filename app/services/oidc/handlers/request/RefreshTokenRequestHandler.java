package services.oidc.handlers.request;

/**
 * Interface for RefreshToken request Handling
 *
 * @author Akila Randil Hettiarachchi
 */
public interface RefreshTokenRequestHandler extends RequestHandler {
}
