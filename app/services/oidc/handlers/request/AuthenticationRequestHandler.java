package services.oidc.handlers.request;

/**
 * Interface for Authorization Code request handling
 *
 * @author Akila Randil Hettiarachchi
 */
public interface AuthenticationRequestHandler extends RequestHandler {
}
