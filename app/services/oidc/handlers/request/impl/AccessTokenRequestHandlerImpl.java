package services.oidc.handlers.request.impl;

import com.google.inject.Inject;
import com.nimbusds.oauth2.sdk.*;
import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import com.nimbusds.openid.connect.sdk.OIDCTokenResponse;
import com.nimbusds.openid.connect.sdk.OIDCTokenResponseParser;
import com.nimbusds.openid.connect.sdk.token.OIDCTokens;
import entity.oidc.BaseTokenResponse;
import entity.oidc.OIDCResponse;
import enumeration.Endpoints;
import enumeration.claims.oauth.OauthValues;
import enumeration.response.error.TokenError;
import enumeration.response.success.AccessTokenValues;
import exception.KouimTestAppException;
import exception.ResponseException;
import play.mvc.Result;
import services.oidc.handlers.request.AccessTokenRequestHandler;
import services.oidc.handlers.request.claims.TokenClaim;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.Objects;

/**
 * Access Token Request Handler Implementation
 * @author Akila Randil Hettiarachchi
 */
public class AccessTokenRequestHandlerImpl implements AccessTokenRequestHandler {

    private TokenClaim tokenClaim;

    @Inject
    public AccessTokenRequestHandlerImpl(TokenClaim tokenClaim) {
        this.tokenClaim = tokenClaim;
    }

    @Override
    public OIDCResponse handleRequest() throws KouimTestAppException {
        TokenRequest request;
        OIDCResponse oidcResponse = null;
        try {
            request = new TokenRequest(
                    new URI(Endpoints.ACCESS_TOKEN_ENDPOINT.toString()),
                    tokenClaim.getClientID(),
                    tokenClaim.getAuthorizationGrant(),
                    Scope.parse(OauthValues.SCOPE.toString()));

            HTTPResponse httpResponse = request.toHTTPRequest().send();
            TokenResponse tokenResponse = OIDCTokenResponseParser.parse(httpResponse);

            if (tokenResponse instanceof TokenErrorResponse) {
                this.errorHandler((TokenErrorResponse) tokenResponse);

            } else if (tokenResponse instanceof OIDCTokenResponse) {

                OIDCTokenResponse oidcTokenResponse = (OIDCTokenResponse) tokenResponse;
                OIDCTokens tokens = oidcTokenResponse.getOIDCTokens();

                if (Objects.equals(tokens.getBearerAccessToken().getValue(), AccessTokenValues.BEARER.toString())) {
                    oidcResponse = new BaseTokenResponse(
                            tokens.getAccessToken(),
                            Instant.now(),
                            tokens.getAccessToken().getLifetime(),
                            tokens.getRefreshToken());
                }
            }
        } catch (URISyntaxException | ParseException | IOException | ResponseException e) {
            throw new KouimTestAppException(e);
        }
        return oidcResponse;

    }

    @Override
    public Result errorHandler(ErrorResponse errorResponse) throws KouimTestAppException {
        ErrorObject errorObject = errorResponse.getErrorObject();
        return TokenError.getCommand(errorObject).exec();
    }


}