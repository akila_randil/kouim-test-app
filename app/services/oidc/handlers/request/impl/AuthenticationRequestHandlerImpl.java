package services.oidc.handlers.request.impl;

import com.google.inject.Inject;
import com.nimbusds.oauth2.sdk.*;
import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import com.nimbusds.oauth2.sdk.id.State;
import com.nimbusds.openid.connect.sdk.*;
import controllers.routes;
import entity.oidc.CodeResponse;
import entity.oidc.OIDCResponse;
import enumeration.Endpoints;
import enumeration.claims.oauth.OauthValues;
import enumeration.response.error.AuthenticationError;
import exception.KouimTestAppException;
import exception.ResponseException;
import play.mvc.Result;
import services.oidc.handlers.request.AuthenticationRequestHandler;
import services.oidc.handlers.request.claims.AuthorizationClaim;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import static play.mvc.Results.redirect;

/**
 * Authentication Request Handler Implementation
 * @author Akila Randil Hettiarachchi
 */
public class AuthenticationRequestHandlerImpl implements AuthenticationRequestHandler {

    private AuthorizationClaim authorizationClaim;
    private State state;
    private Nonce nonce;

    @Inject
    public AuthenticationRequestHandlerImpl(AuthorizationClaim authorizationClaim) throws KouimTestAppException {
        this.authorizationClaim = authorizationClaim;
        state = new State();
        nonce = new Nonce();
    }

    @Override
    public OIDCResponse handleRequest() throws KouimTestAppException {
        AuthenticationRequest request;
        OIDCResponse oidcResponse = null;
        try {
            request = new AuthenticationRequest(
                    new URI(Endpoints.AUTHORIZATION_ENDPOINT.toString()),
                    new ResponseType(OauthValues.RESPONSE_TYPE.toString()),
                    new Scope(OauthValues.SCOPE.toString()),
                    authorizationClaim.getClientID(),
                    authorizationClaim.getRedirectionUri(),
                    state,
                    nonce);
            HTTPResponse httpResponse = request.toHTTPRequest().send();
            AuthenticationResponse response = AuthenticationResponseParser.parse(httpResponse);

            if (response instanceof AuthenticationErrorResponse) {
                if (response.getState().equals(state)) {
                    this.errorHandler((AuthenticationErrorResponse) response);
                } else {
                    this.redirectToLogin();
                    throw new ResponseException("Returned State value does not match the requested state value ");
                    //can redirect to login page as well
                }
            } else if (response instanceof AuthenticationSuccessResponse) {
                if (response.getState().equals(state)) {
                    AuthenticationSuccessResponse successResponse = (AuthenticationSuccessResponse) response;
                    oidcResponse = new CodeResponse(successResponse.getAuthorizationCode());
                } else {
                    this.redirectToLogin();
                    throw new ResponseException("Returned State value does not match the requested state value ");
                    //can redirect to login page as well
                }
            }
        } catch (URISyntaxException | ParseException | IOException e) {
            throw new KouimTestAppException(e);
        }
        return oidcResponse;

    }

    @Override
    public Result errorHandler(ErrorResponse errorResponse) throws KouimTestAppException {
        ErrorObject error = errorResponse.getErrorObject();
        return AuthenticationError.getCommand(error).exec();

    }


    /**
     * Redirect to the login page
     *
     * @return a redirect to the login page
     */
    private Result redirectToLogin() {
        return redirect(routes.ApplicationController.logOut());
    }

}
