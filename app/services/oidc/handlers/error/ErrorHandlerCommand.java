package services.oidc.handlers.error;

import exception.KouimTestAppException;
import play.mvc.Result;

/**
 * An interface that contains a common method for executing an output based on the error code
 * of the OAuth 2.0 responses
 *
 * @author Akila Randil Hettiarachchi
 */
@FunctionalInterface
public interface ErrorHandlerCommand {
    /**
     * Executes the an output based on the error code
     *
     * @return a {@link Result} instance based on the HTTP response code for
     * different types of error code
     * @throws KouimTestAppException if the error code execution fails
     */
    Result exec() throws KouimTestAppException;
}
