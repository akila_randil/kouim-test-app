package services.oidc;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.proc.BadJOSEException;
import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.JWTParser;
import com.nimbusds.jwt.util.DateUtils;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.openid.connect.sdk.Nonce;
import com.nimbusds.openid.connect.sdk.claims.IDTokenClaimsSet;
import com.nimbusds.openid.connect.sdk.validators.IDTokenClaimsVerifier;
import com.nimbusds.openid.connect.sdk.validators.IDTokenValidator;
import enumeration.ResourceURI;
import enumeration.claims.oauth.OauthValues;
import enumeration.response.success.AccessTokenClaim;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;

import static play.mvc.Controller.session;

/**
 * To be implemented
 * @author Akila Randil Hettiarachchi
 */
@Deprecated
public class IDTokenService {
    private final Issuer issuer = new Issuer(ResourceURI.ISSUER.toString());
    private final ClientID clientID = new ClientID(OauthValues.CLIENT_ID.toString());
    private final JWSAlgorithm algorithm = JWSAlgorithm.ES256;
    private Nonce expectedNonce;

    //    Need to know whether id tokens are just jwt or plain or signed or encrypted
//    need to know about the JWK key sets
//    first validate the idtoken then verify the token claimset
//    response type should be here also
    public IDTokenService(Nonce expectedNonce) {
        this.expectedNonce = expectedNonce;


    }


    private IDTokenClaimsSet validateToken() throws MalformedURLException, ParseException, BadJOSEException, JOSEException {
        String idTokenString = session(AccessTokenClaim.ID_TOKEN.toString());
        JWT idToken = JWTParser.parse(idTokenString);
        IDTokenValidator validator = new IDTokenValidator(
                issuer,
                clientID,
                algorithm,
                new URL(""));
        return validator.validate(idToken, expectedNonce);
    }

    private JWTClaimsSet verifyToken() throws ParseException, com.nimbusds.oauth2.sdk.ParseException, BadJOSEException, JOSEException, MalformedURLException {

        IDTokenClaimsSet claimsSet = validateToken();
        IDTokenClaimsVerifier verifier = new IDTokenClaimsVerifier(
                issuer,
                clientID,
                expectedNonce,
                (int) DateUtils.toSecondsSinceEpoch(claimsSet.getIssueTime())
        );
        verifier.verify(claimsSet.toJWTClaimsSet());
        return claimsSet.toJWTClaimsSet();
    }


}
