package services.oidc.impl;

import com.google.inject.Inject;
import com.nimbusds.oauth2.sdk.AuthorizationCode;
import com.nimbusds.oauth2.sdk.AuthorizationCodeGrant;
import com.nimbusds.oauth2.sdk.AuthorizationGrant;
import com.nimbusds.oauth2.sdk.RefreshTokenGrant;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.token.RefreshToken;
import entity.oidc.BaseTokenResponse;
import entity.oidc.CodeResponse;
import enumeration.claims.oauth.OauthValues;
import enumeration.response.success.AccessTokenClaim;
import exception.KouimTestAppException;
import services.oidc.OpenIDService;
import services.oidc.handlers.request.AccessTokenRequestHandler;
import services.oidc.handlers.request.RefreshTokenRequestHandler;
import services.oidc.handlers.request.RequestHandler;
import services.oidc.handlers.request.claims.AuthorizationClaim;
import services.oidc.handlers.request.claims.TokenClaim;
import services.oidc.handlers.request.claims.impl.AuthorizationClaimImpl;
import services.oidc.handlers.request.claims.impl.TokenClaimImpl;
import services.oidc.handlers.request.impl.AccessTokenRequestHandlerImpl;
import services.oidc.handlers.request.impl.AuthenticationRequestHandlerImpl;
import services.oidc.handlers.request.impl.RefreshTokenRequestHandlerImpl;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;

import static play.mvc.Controller.session;


/**
 * Main unit of handling Authorization Code Flow and session management
 *
 * @author Akila Randil Hettiarachchi
 */
public class OpenIDServiceImpl implements OpenIDService {

    private final ClientID clientID = new ClientID(OauthValues.CLIENT_ID.toString());
    private final URI redirectURI;
    private long lifeTime;
    private Instant responseInitTime;
    private BaseTokenResponse tokenResponse;
    private AuthorizationCode code;

    @Inject
    public OpenIDServiceImpl() throws KouimTestAppException {
        try {
            redirectURI = new URI(OauthValues.REDIRECT_URI.toString());
        } catch (URISyntaxException e) {
            throw new KouimTestAppException(e);
        }
    }


    @Override
    public void codeRequest() throws KouimTestAppException {
        AuthorizationClaim claim = new AuthorizationClaimImpl();
        claim.setClientID(clientID);
        claim.setRedirectionUri(redirectURI);
        RequestHandler request = new AuthenticationRequestHandlerImpl(claim);
        CodeResponse codeResponse = (CodeResponse) request.handleRequest();
        code = codeResponse.getCode();
        session("user_session", codeResponse.getCode().getValue());

    }


    @Override
    public void accessTokenRequest() throws KouimTestAppException {

        AuthorizationGrant grant = new AuthorizationCodeGrant(new AuthorizationCode(session("user_session")), redirectURI);
        TokenClaim claim = new TokenClaimImpl();
        claim.setClientID(clientID);
        claim.setAuthorizationGrant(grant);
        AccessTokenRequestHandler request = new AccessTokenRequestHandlerImpl(claim);
        tokenResponse = (BaseTokenResponse) request.handleRequest();
        lifeTime = tokenResponse.getLifeTime();
        responseInitTime = tokenResponse.getResponseInitTime();
        session(AccessTokenClaim.ACCESS_TOKEN.toString(), tokenResponse.getAccessToken().getValue());
        session(AccessTokenClaim.REFRESH_TOKEN.toString(), tokenResponse.getRefreshToken().getValue());
    }


    @Override
    public void refreshTokenRequest() throws KouimTestAppException {
        RefreshToken temp = new RefreshToken(session(AccessTokenClaim.ACCESS_TOKEN.toString()));
        AuthorizationGrant grant = new RefreshTokenGrant(temp);
        TokenClaim claim = new TokenClaimImpl();
        claim.setClientID(clientID);
        claim.setAuthorizationGrant(grant);
        RefreshTokenRequestHandler request = new RefreshTokenRequestHandlerImpl(claim);
        tokenResponse = (BaseTokenResponse) request.handleRequest();
        lifeTime = tokenResponse.getLifeTime();
        responseInitTime = tokenResponse.getResponseInitTime();
        session().put(AccessTokenClaim.ACCESS_TOKEN.toString(), tokenResponse.getAccessToken().getValue());
        session().put(AccessTokenClaim.REFRESH_TOKEN.toString(), tokenResponse.getRefreshToken().getValue());
    }


    public long getLifeTime() {
        return lifeTime;
    }


    public Instant getResponseInitTime() {
        return responseInitTime;
    }

    @Override
    public AuthorizationCode getCode() {
        return code;
    }
}
