package controllers;

import play.mvc.Controller;
import play.mvc.Result;

/**
 * This controller is responsible for the rendering of the main page.
 */
public class HomeController extends Controller {

    /**
     * Rendering of the main page
     * @return a Result
     */
    public Result index() {
        return ok(views.html.main.render());
    }

}
