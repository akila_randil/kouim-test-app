package controllers;

import com.google.inject.Inject;
import entity.ResourceResponse;
import enumeration.ResourceURI;
import exception.KouimTestAppException;
import play.mvc.Controller;
import play.mvc.Result;
import services.ApplicationService;
import services.oidc.OpenIDService;

import java.util.Objects;

/**
 * This controller will be handling the HTTP requests and responses, user login and logout and other
 * application related controllers.
 * @author Akila Randil Hettiarachchi
 */

public class ApplicationController extends Controller {

    private OpenIDService openIDService;
    private ApplicationService applicationService;


    @Inject
    public ApplicationController(OpenIDService openIDService, ApplicationService applicationService) throws KouimTestAppException {
        this.openIDService = openIDService;
        this.applicationService = applicationService;
    }

    /**
     * Redirects the user to the Authorization Server for consent(if needed) to authentication.
     * If the <code>session("user_session")</code> is null, it will request for a new code
     * using that as the user specific session value.
     * if the <code>session("user_session")</code> value matches with the current it will redirect
     * to the landing page.
     * or else the user will be redirected the log out route.
     *
     * @return a Result
     * @throws KouimTestAppException if the process fails
     */
    public Result login() throws KouimTestAppException {
        if (session("user_session") == null) {
            openIDService.codeRequest();

            return ok(ResourceURI.LOGIN.toString());
            //code to redirect to landing page
        } else if (Objects.equals(session("user_session"), openIDService.getCode().getValue())) {
            return redirect(routes.HomeController.index());
            //code to redirect to landing page since use has been already logged in
        } else {
            return redirect(routes.ApplicationController.logOut());
        }

    }

    /**
     * Logs out the user by removing the whole <code>session()</code>
     *
     * @return a Result
     */
    public Result logOut() {
        session().clear();
        return redirect(routes.ApplicationController.login());
        //code to login page
    }

    public Result getRoleMaps() throws KouimTestAppException {
        ResourceResponse resourceResponse = applicationService.request(ResourceURI.ROLE_MAPS.toString());
        String body = resourceResponse.getBody();
        return ok(body);
    }

    public Result getUsers() throws KouimTestAppException {
        ResourceResponse resourceResponse = applicationService.request(ResourceURI.USERS.toString());
        String body;
        body = resourceResponse.getBody();
        return ok(body);
    }


}
