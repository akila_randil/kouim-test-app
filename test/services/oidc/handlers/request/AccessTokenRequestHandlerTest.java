//package services.oidc.handlers.request;
//
//import com.nimbusds.oauth2.sdk.*;
//import com.nimbusds.oauth2.sdk.id.ClientID;
//import com.nimbusds.oauth2.sdk.id.State;
//import com.nimbusds.oauth2.sdk.token.AccessToken;
//import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
//import com.nimbusds.oauth2.sdk.token.RefreshToken;
//import com.nimbusds.openid.connect.sdk.Nonce;
//import com.nimbusds.openid.connect.sdk.token.OIDCTokens;
//import enumeration.Endpoints;
//import enumeration.claims.oauth.OauthValues;
//import TokenError;
//import exception.KouimTestAppException;
//import net.jadler.stubbing.server.jdk.JdkStubHttpServer;
//import org.json.JSONObject;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Rule;
//import org.junit.Test;
//import org.junit.rules.ExpectedException;
//import services.oidc.handlers.error.ErrorHandlerCommand;
//import services.oidc.handlers.request.impl.AccessTokenRequestHandlerImpl;
//
//import java.net.URI;
//
//import static net.jadler.Jadler.*;
//import static org.hamcrest.core.Is.is;
//import static org.junit.Assert.assertThat;
//
///**
// * @author Akila Randil Hettiarachchi
// */
//public class AccessTokenRequestHandlerTest {
//    @Rule
//    public final ExpectedException exception = ExpectedException.none();
//    private State state;
//    private Nonce nonce;
//    private AuthorizationCode code;
//    private ClientID clientID;
//    private AuthorizationGrant authorizationGrant;
//    private AccessTokenRequestHandler requestHandler;
//
//    @Before
//    public void setUp() throws Exception {
//        initJadlerUsing(new JdkStubHttpServer());
//        state = new State();
//        nonce = new Nonce();
//        code = new AuthorizationCode();
//        clientID = new ClientID(OauthValues.CLIENT_ID.toString());
//        authorizationGrant = new AuthorizationCodeGrant(code, new URI(OauthValues.REDIRECT_URI.toString()));
////        requestHandler = new AccessTokenRequestHandlerImpl(clientID, authorizationGrant);
//
//    }
//
//    @After
//    public void tearDown() throws Exception {
//        closeJadler();
//    }
//
//    @Test
//    public void testHandleRequestSuccess() throws Exception {
//
//        AccessToken accessToken = new BearerAccessToken();
//        OIDCTokens tokens = new OIDCTokens("", accessToken, new RefreshToken());
//        JSONObject object = new JSONObject();
//        object.put("access_token", tokens.getAccessToken().getValue());
//        object.put("refresh_token", tokens.getRefreshToken().getValue());
//        object.put("id_token", "");
//        object.put("token_type", "Bearer");
//
//        onRequest()
//                .havingMethodEqualTo("POST")
//                .havingPathEqualTo(Endpoints.ACCESS_TOKEN_ENDPOINT.toString())
//                .havingParameterEqualTo("client_id", OauthValues.CLIENT_ID.toString())
//                .havingParameterEqualTo("grant_type", OauthValues.ACCESS_TOKEN_GRANT_TYPE.toString())
//                .havingParameterEqualTo("code", code.getValue())
//                .havingParameterEqualTo("redirect_uri", OauthValues.REDIRECT_URI.toString())
//
//                .respond()
//                .withContentType("application/json")
//                .withBody(tokens.toJSONObject().toJSONString());
//
//        assertThat(tokens.toJSONObject().toJSONString(), is(object.toString()));
//    }
//
//    @Test
//    public void testHandleRequestError() throws Exception {
//
//        ErrorObject errorObject = new ErrorObject("invalid_request");
//
//        onRequest()
//                .havingMethodEqualTo("POST")
//                .havingPathEqualTo(Endpoints.ACCESS_TOKEN_ENDPOINT.toString())
//                .havingParameterEqualTo("client_id", OauthValues.CLIENT_ID.toString())
//                .havingParameterEqualTo("grant_type", OauthValues.ACCESS_TOKEN_GRANT_TYPE.toString())
//                .havingParameterEqualTo("code", code.getValue())
//                .havingParameterEqualTo("redirect_uri", OauthValues.REDIRECT_URI.toString())
//
//                .respond()
//                .withContentType("application/json")
//                .withStatus(400)
//                .withBody(errorObject.toJSONObject().toJSONString());
//
//
//        ErrorHandlerCommand expected = TokenError.INVALID_REQUEST.getCommand();
//        assertThat(TokenError.getCommand(errorObject), is(expected));
//    }
//
//    @Test
//    public void testHandleRequestWithException() throws Exception {
//        exception.expect(KouimTestAppException.class);
//        requestHandler.handleRequest();
//    }
//
////    @Test
////    public void testErrorHandler() throws Exception {
////        ErrorObject errorObject = new ErrorObject("invalid_request");
////        ErrorResponse errorResponse = new TokenErrorResponse(errorObject);
////        requestHandler.errorHandler(errorResponse);
////    }
////
////    @Test
////    public void testErrorHandlerWithException() throws Exception {
////        exception.expect(KouimTestAppException.class);
////        ErrorObject errorObject = new ErrorObject("");
////        ErrorResponse errorResponse = new TokenErrorResponse(errorObject);
////        requestHandler.errorHandler(errorResponse);
////    }
//
//
//}