//package services.oidc.handlers.request;
//
//import com.nimbusds.oauth2.sdk.ErrorObject;
//import com.nimbusds.oauth2.sdk.ErrorResponse;
//import com.nimbusds.oauth2.sdk.RefreshTokenGrant;
//import com.nimbusds.oauth2.sdk.TokenErrorResponse;
//import com.nimbusds.oauth2.sdk.id.ClientID;
//import com.nimbusds.oauth2.sdk.token.AccessToken;
//import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
//import com.nimbusds.oauth2.sdk.token.RefreshToken;
//import com.nimbusds.openid.connect.sdk.token.OIDCTokens;
//import enumeration.Endpoints;
//import enumeration.claims.oauth.OauthValues;
//import exception.KouimTestAppException;
//import net.jadler.stubbing.server.jdk.JdkStubHttpServer;
//import org.json.JSONObject;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Rule;
//import org.junit.Test;
//import org.junit.rules.ExpectedException;
//import services.oidc.handlers.request.impl.RefreshTokenRequestHandlerImpl;
//
//import static net.jadler.Jadler.closeJadler;
//import static net.jadler.Jadler.initJadlerUsing;
//import static net.jadler.Jadler.onRequest;
//import static org.hamcrest.core.Is.is;
//import static org.junit.Assert.assertThat;
//
///**
// * @author Akila Randil Hettiarachchi
// */
//public class RefreshTokenRequestHandlerTest {
//    @Rule
//    public final ExpectedException exception = ExpectedException.none();
//    private RefreshTokenRequestHandler requestHandler;
//
//    @Before
//    public void setUp() throws Exception {
//        initJadlerUsing(new JdkStubHttpServer());
////        requestHandler = new RefreshTokenRequestHandlerImpl(
////                new ClientID(OauthValues.CLIENT_ID.toString()),
////                new RefreshTokenGrant(new RefreshToken("123")));
//    }
//
//    @After
//    public void tearDown() throws Exception {
//        closeJadler();
//    }
//
//    @Test
//    public void testHandleRequest() throws Exception {
//
//        AccessToken accessToken = new BearerAccessToken();
//        OIDCTokens tokens = new OIDCTokens("", accessToken, new RefreshToken());
//        JSONObject object = new JSONObject();
//
//        object.put("access_token", tokens.getAccessToken().getValue());
//        object.put("refresh_token", tokens.getRefreshToken().getValue());
//        object.put("id_token", "");
//        object.put("token_type", "Bearer");
//
//        onRequest()
//                .havingMethodEqualTo("POST")
//                .havingPathEqualTo(Endpoints.ACCESS_TOKEN_ENDPOINT.toString())
//                .havingParameterEqualTo("client_id", OauthValues.CLIENT_ID.toString())
//                .havingParameterEqualTo("grant_type", OauthValues.REFRESH_TOKEN_GRANNT_TYPE.toString())
//                .havingParameterEqualTo("refresh_token", "123")
//                .havingParameterEqualTo("scope", OauthValues.SCOPE.toString())
//
//                .respond()
//                .withContentType("application/json")
//                .withBody(tokens.toJSONObject().toJSONString());
//
//        assertThat(tokens.toJSONObject().toJSONString(), is(object.toString()));
//    }
//
////    @Test
////    public void testHandleRequestWithException() throws Exception {
////        exception.expect(KouimTestAppException.class);
////        requestHandler.handleRequest();
////    }
//
////    @Test
////    public void testErrorHandler() throws Exception {
////        ErrorObject errorObject = new ErrorObject("invalid_request");
////        ErrorResponse errorResponse = new TokenErrorResponse(errorObject);
////        requestHandler.errorHandler(errorResponse);
////    }
//
////    @Test
////    public void testErrorHandlerWithException() throws Exception {
////        exception.expect(KouimTestAppException.class);
////        ErrorObject errorObject = new ErrorObject("");
////        ErrorResponse errorResponse = new TokenErrorResponse(errorObject);
////        requestHandler.errorHandler(errorResponse);
////    }
//}