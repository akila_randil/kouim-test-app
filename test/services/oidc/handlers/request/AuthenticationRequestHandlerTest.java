//package services.oidc.handlers.request;
//
//import com.nimbusds.oauth2.sdk.AuthorizationCode;
//import com.nimbusds.oauth2.sdk.ErrorObject;
//import com.nimbusds.oauth2.sdk.ErrorResponse;
//import com.nimbusds.oauth2.sdk.TokenErrorResponse;
//import com.nimbusds.oauth2.sdk.id.ClientID;
//import com.nimbusds.oauth2.sdk.id.State;
//import com.nimbusds.openid.connect.sdk.Nonce;
//import enumeration.Endpoints;
//import enumeration.claims.oauth.OauthValues;
//import AuthenticationError;
//import exception.KouimTestAppException;
//import net.jadler.stubbing.server.jdk.JdkStubHttpServer;
//import org.apache.http.NameValuePair;
//import org.apache.http.client.utils.URIBuilder;
//import org.apache.http.client.utils.URLEncodedUtils;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Rule;
//import org.junit.Test;
//import org.junit.rules.ExpectedException;
//import services.oidc.OpenIDService;
//import services.oidc.handlers.error.ErrorHandlerCommand;
//import services.oidc.handlers.request.impl.AuthenticationRequestHandlerImpl;
//
//import java.net.URI;
//import java.net.URISyntaxException;
//import java.util.List;
//
//import static net.jadler.Jadler.*;
//import static org.hamcrest.core.Is.is;
//import static org.hamcrest.core.IsNot.not;
//import static org.junit.Assert.assertThat;
//
///**
// * @author Akila Randil Hettiarachchi
// */
//public class AuthenticationRequestHandlerTest {
//
//    @Rule
//    public final ExpectedException exception = ExpectedException.none();
//    private State state;
//    private Nonce nonce;
//    private AuthorizationCode code;
//    private AuthenticationRequestHandler requestHandler;
//
//    @Before
//    public void setUp() throws Exception {
//        ClientID clientID = new ClientID(OauthValues.CLIENT_ID.toString());
//        URI redirectURI = new URI(OauthValues.REDIRECT_URI.toString());
////        requestHandler = new AuthenticationRequestHandlerImpl(clientID, redirectURI);
//        initJadlerUsing(new JdkStubHttpServer());
//        state = new State();
//        nonce = new Nonce();
//        code = new AuthorizationCode();
//
//    }
//
//    @After
//    public void tearDown() throws Exception {
//        closeJadler();
//    }
//
//    @Test
//    public void testHandleRequestSuccess() throws Exception {
//
//        URI responseURI = new URIBuilder()
//                .setScheme("https")
//                .setHost(OauthValues.REDIRECT_URI.toString())
//                .setParameter("code", code.getValue())
//                .setParameter("state", state.getValue()).build();
//
//        onRequest()
//                .havingMethodEqualTo("GET")
//                .havingPathEqualTo(Endpoints.AUTHORIZATION_ENDPOINT.toString())
//                .havingParameterEqualTo("response_type", OauthValues.RESPONSE_TYPE.toString())
//                .havingParameterEqualTo("client_id", OauthValues.CLIENT_ID.toString())
//                .havingParameterEqualTo("redirect_uri", OauthValues.REDIRECT_URI.toString())
//                .havingParameterEqualTo("state", state.getValue())
//                .havingParameterEqualTo("nonce", nonce.getValue())
//
//                .respond()
//                .withHeader("location", responseURI.toString())
//                .withStatus(302);
//
//        assertThat(state.getValue(), is(getParamValue(responseURI, "state")));
//    }
//
//    @Test
//    public void testHandleRequestSuccessStateNotEquals() throws URISyntaxException, KouimTestAppException {
//        URI responseURI = new URIBuilder()
//                .setScheme("https")
//                .setHost(OauthValues.REDIRECT_URI.toString())
//                .setParameter("code", code.getValue())
//                .setParameter("state", state.getValue())
//                .build();
//
//        onRequest()
//                .havingMethodEqualTo("GET")
//                .havingPathEqualTo(Endpoints.AUTHORIZATION_ENDPOINT.toString())
//                .havingParameterEqualTo("response_type", OauthValues.RESPONSE_TYPE.toString())
//                .havingParameterEqualTo("client_id", OauthValues.CLIENT_ID.toString())
//                .havingParameterEqualTo("redirect_uri", OauthValues.REDIRECT_URI.toString())
//                .havingParameterEqualTo("state", state.getValue())
//                .havingParameterEqualTo("nonce", nonce.getValue())
//                .respond()
//                .withHeader("location", responseURI.toString())
//                .withStatus(302);
//
//        assertThat(state.getValue(), is(getParamValue(responseURI, "state")));
//        exception.expect(KouimTestAppException.class);
//        requestHandler.handleRequest();
//
//    }
//
//    @Test
//    public void testHandlerRequestErrorStateEquals() throws Exception {
//        URI responseURI = new URIBuilder()
//                .setScheme("https")
//                .setHost(OauthValues.REDIRECT_URI.toString())
//                .setParameter("error", "invalid_request")
//                .setParameter("state", state.getValue())
//                .build();
//
//        onRequest()
//                .havingMethodEqualTo("GET")
//                .havingPathEqualTo(Endpoints.AUTHORIZATION_ENDPOINT.toString())
//                .havingParameterEqualTo("response_type", OauthValues.RESPONSE_TYPE.toString())
//                .havingParameterEqualTo("client_id", OauthValues.CLIENT_ID.toString())
//                .havingParameterEqualTo("redirect_uri", OauthValues.REDIRECT_URI.toString())
//                .havingParameterEqualTo("state", state.getValue())
//                .havingParameterEqualTo("nonce", nonce.getValue())
//                .respond()
//                .withHeader("location", responseURI.toString())
//                .withStatus(302);
//
//        assertThat(state.getValue(), is(getParamValue(responseURI, "state")));
//        ErrorObject errorObject = new ErrorObject("invalid_request");
//        ErrorHandlerCommand expected = AuthenticationError.INVALID_REQUEST.getCommand();
//        assertThat(AuthenticationError.getCommand(errorObject), is(expected));
//    }
//
//    @Test
//    public void testHandlerRequestErrorStateNotEquals() throws Exception {
//        URI responseURI = new URIBuilder()
//                .setScheme("https")
//                .setHost(OauthValues.REDIRECT_URI.toString())
//                .setParameter("error", "invalid_request")
//                .setParameter("state", "123")
//                .build();
//
//        onRequest()
//                .havingMethodEqualTo("GET")
//                .havingPathEqualTo(Endpoints.AUTHORIZATION_ENDPOINT.toString())
//                .havingParameterEqualTo("response_type", OauthValues.RESPONSE_TYPE.toString())
//                .havingParameterEqualTo("client_id", OauthValues.CLIENT_ID.toString())
//                .havingParameterEqualTo("redirect_uri", OauthValues.REDIRECT_URI.toString())
//                .havingParameterEqualTo("state", state.getValue())
//                .havingParameterEqualTo("nonce", nonce.getValue())
//                .respond()
//                .withHeader("location", responseURI.toString())
//                .withStatus(302);
//
//        assertThat(state.getValue(), not(getParamValue(responseURI, "state")));
//        exception.expect(KouimTestAppException.class);
//        requestHandler.handleRequest();
//    }
//
//    @Test
//    public void testErrorHandler() throws Exception {
//        ErrorObject errorObject = new ErrorObject("invalid_request");
//        ErrorResponse errorResponse = new TokenErrorResponse(errorObject);
//        requestHandler.errorHandler(errorResponse);
//    }
//
//    @Test
//    public void testErrorHandlerWithException() throws Exception {
//        exception.expect(KouimTestAppException.class);
//        ErrorObject errorObject = new ErrorObject("");
//        ErrorResponse errorResponse = new TokenErrorResponse(errorObject);
//        requestHandler.errorHandler(errorResponse);
//    }
//
//    private String getParamValue(URI uri, String key) {
//        String val = null;
//        List<NameValuePair> params = URLEncodedUtils.parse(uri, "utf8");
//
//        for (NameValuePair pair : params) {
//            String name = pair.getName();
//            if (name.equals(key)) {
//                val = pair.getValue();
//            }
//        }
//        return val;
//    }
//}