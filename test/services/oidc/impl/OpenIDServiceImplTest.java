package services.oidc.impl;

import com.nimbusds.oauth2.sdk.AuthorizationCode;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.oauth2.sdk.token.RefreshToken;
import entity.oidc.BaseTokenResponse;
import entity.oidc.CodeResponse;
import org.junit.Before;
import org.junit.Test;
import play.api.mvc.Handler;
import services.oidc.handlers.request.AccessTokenRequestHandler;
import services.oidc.handlers.request.AuthenticationRequestHandler;
import services.oidc.handlers.request.RefreshTokenRequestHandler;
import services.oidc.handlers.request.RequestHandler;
import services.oidc.handlers.request.claims.AuthorizationClaim;
import services.oidc.handlers.request.impl.AuthenticationRequestHandlerImpl;

import java.time.Instant;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Akila Randil Hettiarachchi
 */
public class OpenIDServiceImplTest {

    private CodeResponse codeResponse;
    private BaseTokenResponse tokenResponse;

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void testCodeRequest() throws Exception {
        codeResponse = new CodeResponse(new AuthorizationCode("123"));
        AuthenticationRequestHandler handler = mock(AuthenticationRequestHandler.class);
        when(handler.handleRequest()).thenReturn(codeResponse);
    }

    @Test
    public void testAccessTokenRequest() throws Exception {
        tokenResponse = new BaseTokenResponse(new BearerAccessToken("access"), Instant.now(), 123L, new RefreshToken());
        AccessTokenRequestHandler handler = mock(AccessTokenRequestHandler.class);
        when(handler.handleRequest()).thenReturn(tokenResponse);
    }

    @Test
    public void testRefreshTokenRequest() throws Exception {
        tokenResponse = new BaseTokenResponse(new BearerAccessToken("New access"), Instant.now(), 123L, new RefreshToken());
        RefreshTokenRequestHandler handler = mock(RefreshTokenRequestHandler.class);
        when(handler.handleRequest()).thenReturn(tokenResponse);
    }
}