package services;

import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import entity.ResourceResponse;
import org.junit.Before;
import org.junit.Test;
import services.oidc.OpenIDService;
import services.impl.ApplicationServiceTaskImpl;

import java.io.IOException;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Akila Randil Hettiarachchi
 */

public class ApplicationServiceImplTaskTest {

    private OpenIDService openIDService;
    private ApplicationServiceTaskImpl task;
    private HTTPRequest httpRequest;
    private HTTPResponse httpResponse;

    @Before
    public void setUp() throws Exception {
        openIDService = mock(OpenIDService.class);
        httpRequest = mock(HTTPRequest.class);
        httpResponse = mock(HTTPResponse.class);
        task = new ApplicationServiceTaskImpl(openIDService);
    }

    @Test
    public void testGetDataWhenAccessTokenSessionIsNull() throws Exception {

        ResourceResponse resourceResponse = new ResourceResponse();
        resourceResponse.setBody("testUsers");
        resourceResponse.setStatus(200);

        httpResponse.setContent(resourceResponse.getBody());
        when(httpResponse.getContent()).thenReturn(resourceResponse.getBody());

    }

    @Test
    public void testGetDataIOException() throws Exception {
        ResourceResponse resourceResponse = new ResourceResponse();
        resourceResponse.setBody("testUsers");
        resourceResponse.setStatus(200);

        when(httpResponse.getContent()).thenThrow(IOException.class);

    }
}