package services;

import entity.ResourceResponse;
import exception.KouimTestAppException;
import org.junit.Before;
import org.junit.Test;
import services.impl.ApplicationServiceImpl;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Akila Randil Hettiarachchi
 */
public class ApplicationServiceImplTest {

    private ApplicationServiceImpl service;
    private ApplicationServiceTask task;

    @Before
    public void setUp() throws Exception {
        task = mock(ApplicationServiceTask.class);
        service = new ApplicationServiceImpl(task);
    }


    @Test
    public void testRequest() throws Exception {

        ResourceResponse resourceResponse = new ResourceResponse();
        resourceResponse.setBody("testBody");
        resourceResponse.setStatus(200);
        task.setUrl("testUrl");
        when(task.getData()).thenReturn(resourceResponse);

        assertThat(task.getData(), is(resourceResponse));
    }

    @Test
    public void testErrorRequest() throws Exception {

        task.setUrl("testURl");
        when(task.getData()).thenThrow(KouimTestAppException.class);

    }
}