package controllers;

import com.nimbusds.oauth2.sdk.AuthorizationCode;
import entity.ResourceResponse;
import enumeration.ResourceURI;
import net.jadler.stubbing.server.jdk.JdkStubHttpServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import play.Application;
import play.inject.guice.GuiceApplicationBuilder;
import play.mvc.Http;
import play.mvc.Result;
import play.test.WithApplication;
import services.ApplicationService;
import services.ApplicationServiceTask;
import services.oidc.OpenIDService;
import services.oidc.handlers.request.AuthenticationRequestHandler;
import services.oidc.handlers.request.impl.AuthenticationRequestHandlerImpl;

import static net.jadler.Jadler.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static play.mvc.Controller.session;
import static play.mvc.Http.Status.OK;
import static play.mvc.Results.ok;
import static play.test.Helpers.fakeRequest;
import static play.test.Helpers.route;

/**
 * @author Akila Randil Hettiarachchi
 */

public class ApplicationServiceControllerTest extends WithApplication {

    protected Application application;


    @Mock
    private ApplicationService appService;

    @Mock
    private ApplicationServiceTask task;

    @Mock
    private OpenIDService openIDService;

    @InjectMocks
    private ApplicationController controller;

    @Override
    protected Application provideApplication() {
        return new GuiceApplicationBuilder()
                .build();
    }

    @Before
    public void setUp() throws Exception {
        application = provideApplication();

        openIDService = mock(OpenIDService.class);
        appService = mock(ApplicationService.class);
        controller = new ApplicationController(openIDService, appService);
        initJadlerUsing(new JdkStubHttpServer());

    }

    @After
    public void tearDown() throws Exception {
        closeJadler();

    }

    @Test
    public void testGetRoleMaps() throws Exception {
        ResourceResponse resourceResponse = new ResourceResponse();
        resourceResponse.setBody("testBody");
        resourceResponse.setStatus(200);

        when(appService.request(ResourceURI.ROLE_MAPS.toString())).thenReturn(resourceResponse);

        assertEquals(OK, resourceResponse.getStatus());
    }

    @Test
    public void testGetUserRoles() throws Exception {
        ResourceResponse resourceResponse = new ResourceResponse();
        resourceResponse.setBody("testUsers");
        resourceResponse.setStatus(200);

        when(appService.request(ResourceURI.USERS.toString())).thenReturn(resourceResponse);

        assertEquals(OK, resourceResponse.getStatus());

    }

    @Test
    public void testLogin() throws Exception {


    }

    @Test
    public void testLogOut() throws Exception {


    }


    @Test
    public void testGetUsers() throws Exception {

    }


}
//        when(task.getData()).thenReturn(resourceResponse);
//        when(appService.request("https://www.google.com")).thenReturn(resourceResponse);

//        Http.RequestBuilder request = new Http.RequestBuilder()
//                .method("GET")
//                .bodyText(resourceResponse.getBody())
//                .session("user_session", "esf46546")
//                .session("access_token", "44646464");
//        Http.Context.current.set(new Http.Context(request));
