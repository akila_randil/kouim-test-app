name := """KOUIM Test App"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs
)


libraryDependencies += "com.nimbusds" % "oauth2-oidc-sdk" % "5.14"
libraryDependencies += "org.glassfish.jersey.core" % "jersey-client" % "2.23.1"
libraryDependencies += "net.jadler" % "jadler-all" % "1.3.0"
libraryDependencies += "net.jadler" % "jadler-jdk" % "1.3.0"
libraryDependencies += "org.mockito" % "mockito-all" % "1.10.19"
libraryDependencies += "org.json" % "json" % "20160810"
libraryDependencies += "org.powermock" % "powermock-module-junit4" % "1.6.5"

